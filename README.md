# Exoplanet ML
This is a machine learning project for detecting exoplanets using transit survey based light curves.

# Important Links
* https://www.rasgoml.com/feature-engineering-tutorials/how-to-create-time-series-features-with-tsfresh
* https://exoplanetarchive.ipac.caltech.edu/docs/acknowledge.html
* https://exoplanetarchive.ipac.caltech.edu/docs/doi.html
* https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=kep_conf_names
* https://exoplanetarchive.ipac.caltech.edu/docs/table-redirect.html


![Exoplanet](/Images/Hr8799_orbit_hd.gif/)

## Feature Engineering Techniques

* https://www.analyticsvidhya.com/blog/2020/10/feature-selection-techniques-in-machine-learning/
* https://www.javatpoint.com/feature-selection-techniques-in-machine-learning
* https://www.simplilearn.com/tutorials/machine-learning-tutorial/feature-selection-in-machine-learning
* https://machinelearningmastery.com/feature-selection-with-real-and-categorical-data/
